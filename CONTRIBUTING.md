
# Contributing to go-stride

## Code of Conduct
This project is bound by a [Code of Conduct][CODEOFCONDUCT].

## Reporting Issues

This section guides you through submitting a bug report for go-stride. Following these guidelines helps us and the community understand your issue, reproduce the behavior, and find related issues.

When you are creating an issue, please include as many details as possible. Fill out [the required template](ISSUE_TEMPLATE.md), the information it asks helps us resolve issues faster.

### Before submitting an issue

* **Perform a [cursory search][IssueTracker]** to see if the problem has already been reported. If it has, add a comment to the existing issue instead of opening a new one.

### How do I submit a (good) issue?

* **Use a clear and descriptive title** for the issue to identify the problem.
* **Describe the exact steps which reproduce the problem** in as many details as possible. When listing steps, **don't just say what you did, but explain how you did it**. For example, if you opened a inline dialog, explain if you used the mouse, or a keyboard shortcut.
* **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened and share more information using the guidelines below.

Include details about your configuration and environment:

* **Which OS are you running on?**
* **What version of golang are you using**?

### Code Contributions

#### Why should I contribute?

1. While we strive to look at new issues as soon as we can, because of the many priorities we juggle and limited resources, issues raised often don't get looked into soon enough.
2. We want your contributions. We are always trying to improve our docs, processes and tools to make it easier to submit your own changes.
3. At Atlassian, "Play, As A Team" is one of our values. We encourage cross team contributions and collaborations.

Please raise a new issue [here][IssueTracker].

### Follow code style guidelines

It is recommended you use the git hooks found in the misc directory, this will include go-fmt

## Merge into master
All new feature code must be completed in a feature branch and have a corresponding Feature or Bug issue in the go-stride project.

Once you are happy with your changes, you must push your branch to Bitbucket and create a pull request. All pull requests must have at least 2 reviewers from the go-stride team. Once the pull request has been approved it may be merged into master.

Each PR should consist of exactly one commit, use git rebase and squash, and should be as small as possible. If you feel multiple commits are warrented you should probably be filing them as multiple PRs.

**Attention! Achtung! BÐ½Ð¸Ð¼aÐ½Ð¸Ðµ! AtenciÃ³n! à¤§à¥à¤¯à¤¾à¤¨! æ³¨æ„!**: *Merging into master will automatically release a component. See below for more details*

## Release a component
Releasing components is completely automated. The process of releasing will begin when changes are made to the `master` branch:

* Pipelines will move the go branch forward after successful build on master. This will change the version aquired by go-get
* New pact tests will be sent to the Stride pact server.

## Root dependencies

go-stride endeavours to avoid dependencies outside the SDK whenever possible. Any dependencies that do get introduced should be vendored with the deps package manager.

## Make changes to the Pipelines build
go-stride uses Bitbucket Pipelines for it's continuous integration. The build scripts are defined in `bitbucket-pipelines.yml`.

[CODEOFCONDUCT]: ./CODE_OF_CONDUCT.md
[IssueTracker]: https://bitbucket.org/atlassian/go-stride/issues?status=new&status=open
