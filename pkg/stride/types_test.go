package stride

import (
	"encoding/json"
	"testing"
)

type structuredDocumentNode interface {
	//Type() inlineGroupNode
	json.Marshaler
	//json.Unmarshaler
}

func Test_document_objects_implement_interfaces(t *testing.T) {
	// force a compile time type check that objects implement the serialization methods
	var _ structuredDocumentNode = &Document{}
	var _ structuredDocumentNode = &Paragraph{}
	var _ structuredDocumentNode = &Mention{}
	var _ structuredDocumentNode = &Emoji{}
	var _ structuredDocumentNode = &HardBreak{}
	var _ structuredDocumentNode = &ApplicationCard{}

	// not sure if these are needed :P
	//var _ structuredDocumentNode = &Lozenge{}
	//var _ structuredDocumentNode = &Badge{}
	//var _ structuredDocumentNode = &Mark{}
}
