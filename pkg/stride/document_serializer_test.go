package stride

import (
	"encoding/json"
	"reflect"
	"testing"
)

func Test_Deserialize_Documents(t *testing.T) {
	cases := []struct {
		bs   []byte
		text string
		p    *Payload
	}{
		{
			bs:   []byte(`{"body":{"type":"doc","version":1,"content":[{"type":"paragraph","content":[{"type":"text","text":"The gorp builds are failing again #hashtag."}]}]}}`),
			text: "The gorp builds are failing again #hashtag.",
			p: &Payload{
				Body: &Document{
					Version: 1,
					Content: []interface{}{
						&Paragraph{
							Content: []InlineGroupNode{
								&Text{
									Text: "The gorp builds are failing again #hashtag.",
								},
							},
						},
					},
				},
			},
		},

		{
			bs:   []byte(`{"body":{"type":"doc","version":1,"content":[{"type":"paragraph","content":[{"type":"text","text":"The gorp builds are failing again #hashtag.","marks":[{"type":"link","attrs":{"href":"https://google.com"}}]}]}]}}`),
			text: "The gorp builds are failing again #hashtag.",
			p: &Payload{
				Body: &Document{
					Version: 1,
					Content: []interface{}{
						&Paragraph{
							Content: []InlineGroupNode{
								&Text{
									Text: "The gorp builds are failing again #hashtag.",
									Marks: []Mark{
										&Link{
											Href: "https://google.com",
										},
									},
								},
							},
						},
					},
				},
			},
		},

		{
			bs:   []byte("{\"body\":{\"type\":\"doc\",\"version\":1,\"content\":[{\"type\":\"applicationCard\",\"attrs\":{\"collapsible\":false,\"description\":{\"text\":\"Description\"},\"link\":{\"url\":\"http://google.com\"},\"text\":\"\",\"title\":{\"text\":\"Title\"}}}]}}"),
			text: "Title\n\nDescription\n",
			p: &Payload{
				Body: &Document{
					Version: 1,
					Content: []interface{}{
						&ApplicationCard{
							Attrs: &ApplicationCardAttrs{
								Text:        "",
								Link:        &URL{"http://google.com"},
								Description: &CardAttrText{"Description"},
								Title:       CardAttrTitle{Text: "Title"},
							},
						},
					},
				},
			},
		},
	}

	for i, testCase := range cases {
		// we wish to assert that:
		//   compose(marshal, unmarshal) == identity<payload>
		//   compose(unmarshal, marshal) == identity<string
		var bs []byte
		var err error

		intermediate := &Payload{}
		err = json.Unmarshal(testCase.bs, &intermediate)
		if err != nil {
			t.Error("index [ ", i, " ] ", err.Error())
		}
		bs, err = json.Marshal(&intermediate)
		if err != nil {
			t.Error("index [ ", i, " ] ", err.Error())
		}

		if string(bs) != string(testCase.bs) {
			t.Error(
				"A) Marshal(Unmarshal(p)) failed for test case [ ", i, " ]",
				"\nExpected: ", string(testCase.bs),
				"\nActual:   ", string(bs))
		}

		//////////////////////////////////////
		actual := &Payload{}
		bs, err = json.Marshal(testCase.p)
		if err != nil {
			t.Error("index [ ", i, " ] ", err.Error())
		}
		err = json.Unmarshal(bs, &actual)
		if err != nil {
			t.Error("index [ ", i, " ] ", err.Error())
		}

		if !reflect.DeepEqual(testCase.p, actual) {
			s, _ := json.Marshal(actual)
			t.Error(
				"B) Unmarshal(Marshal(p)) failed for test case [ ", i, " ]",
				"\nExpected: ", string(testCase.bs),
				"\nActual:   ", string(s))
		}

		/////////////////////////////////////
		asText := testCase.p.AsText()
		if asText != testCase.text {
			t.Error("For index [ ", i, " ] AsText(p) ",
				"\nExpected: ", testCase.text,
				"\nGot       ", asText)
		}
	}
}

func Test_sanityOfEmptySlices(t *testing.T) {
	x := struct {
		xs []int
	}{}

	y := struct {
		ys []int
	}{
		ys: make([]int, 0),
	}

	if reflect.DeepEqual(x, y) {
		t.Error("Nil slice == empty slice, since when?")
	}
}
