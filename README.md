# go-stride

A golang [Stride](https://stride.com) client.

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

Please see the [Changelog][CHANGELOG] for information about releases and pending changes.

**Note that the API is subject to breaking changes prior to v1.0.0 release.**

# Getting Started

If this is for a new app you'll need to register it in [Stride Developer Central](https://developer.atlassian.com/apps)

This will get you a client ID and secret, which are used by the go-stride client.
Here's a quick start main function:

```
func main() {
	clientID := os.Getenv("STRIDE_API_CLIENT_ID")
	clientSecret := os.Getenv("STRIDE_API_SECRET")
	webapp := &WebApp{
		client: stride.New(clientID, clientSecret),
	}
	mux := buildHandler(webapp)
	http.ListenAndServe("0.0.0.0:8080", mux)
}

```

Endpoints that will be contacted by stride should be wrapped with RequireTokenMiddleware.
This provides jwt authentication using your key and secret.
```
func buildHandler(wa *WebApp) *http.ServeMux {
	r := http.NewServeMux()
	r.Handle("/bot-mention", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.botMention)))
	return r
}
```
The first client call requiring a token will fetch one from the Atlassian auth API and cache it until near expiry.
In order for stride to send you messages you'll need to install an
[app descriptor](https://developer.atlassian.com/cloud/stride/blocks/app-descriptor/) in [Stride Developer Central](https://developer.atlassian.com/apps)

For a complete example see the [Hello-Bot example][HELLOBOT]

# Exploring the Code Base

* document types use pointers to signal fields are optional
* object types use minimal members, convenience functions are functions (e.x. SendTextReply)
* external library objects are accepted as a minimal interface (e.x. NewClient(...., HttpClient)

# Reporting issues

We believe in open contributions and the power of a strong development community. Please read our [Contributing guidelines][CONTRIBUTING] on how to contribute back and report issues to go-stride.

# Contributors

Pull requests, issues and comments are welcomed. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests
* Read [Contributing guidelines][CONTRIBUTING] for more details

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

# License (Apache v2.0)

Copyright @ 20xx - 20xx Atlassian Pty Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[CHANGELOG]: ./CHANGELOG.md
[CONTRIBUTING]: ./CONTRIBUTING.md
[LICENSE]: ./LICENSE.txt
[CODEOFCONDUCT]: ./CODE_OF_CONDUCT.md
[HELLOBOT]: ./cmd/examples/hellobot/hellobot.go
