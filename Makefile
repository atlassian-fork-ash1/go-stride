
all: install-hooks test

install-hooks:
	@misc/scripts/install-hooks

dep:
	@misc/scripts/deps-ensure
	@dep init

test:
	@go test ./...

test-ci:
	@misc/scripts/test_with_threshold


## for code gen / peformance and other things that the project doesn't actually need to compile

tooling:
	@go get github.com/golang/mock/gomock
	@go get github.com/golang/mock/mockgen

## experimental, still requires some work by hand :(

mock-gen:
	@mockgen \
		-source=pkg/stride/stride.go \
		-destination=pkg/mock_stride/mock_stride.go \
