package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"bitbucket.org/atlassian/go-stride/pkg/stride"
	"strings"
)

type WebApp struct {
	client stride.Client
}

func buildHandler(wa *WebApp) *http.ServeMux {
	r := http.NewServeMux()
	r.HandleFunc("/healthcheck", wa.healthcheck)
	r.HandleFunc("/descriptor", wa.descriptor)
	// Authenticated endpoints require a Bearer auth token which we validate.
	r.Handle("/installed", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.installed)))
	r.Handle("/bot-mention", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.botMention)))
	r.HandleFunc("/", wa.helloWorld)
	return r
}

func (wa *WebApp) healthcheck(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	w.Write([]byte("OK"))
}

func (wa *WebApp) helloWorld(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	w.Write([]byte("Hello, World!"))
}

func (wa *WebApp) descriptor(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	descriptor := &stride.Descriptor{
		BaseURL: "https://" + req.Host,
		Key:     "helloBot",
		LifeCycle: &stride.LifeCycle{
			Installed:   "/installed",
			Uninstalled: "/uninstalled",
		},
		Modules: map[stride.ModuleType][]stride.Module{
			stride.ModuleBot: {
				&stride.Bot{
					Key: "hello-bot",
					Mention: &stride.URL{
						URL: "/bot-mention",
					},
				},
			},
		},
	}
	b, err := json.Marshal(descriptor)
	if err != nil {
		// Failed to render descriptor. Should not happen.
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write(b)
}

func (wa *WebApp) installed(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Could not read request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// log.Println(string(b))
	var d struct {
		CloudID    string `json:"cloudId"`
		ResourceID string `json:"resourceId"`
	}
	err = json.Unmarshal(b, &d)
	if err != nil {
		log.Printf("Failed to decode json: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = stride.SendText(wa.client, d.CloudID, d.ResourceID, "Hi there! Thanks for adding me to this conversation. To see me in action, just mention me in a message")
	if err != nil {
		log.Printf("Failed to send message: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println("App installed in a conversation")
	w.Write([]byte("Installed"))
}

func (wa *WebApp) botMention(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	log.Println("Received a mention")

	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Could not read request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	m := &stride.Message{}
	err = json.Unmarshal(b, m)
	if err != nil {
		log.Printf("Failed to decode json: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if strings.Index(m.Message.Text, "Hello") >= 0 {
		err = stride.SendTextReply(wa.client, m, "Hey, what's up? (Sorry, that's all I can do)")
	} else {
		err = stride.SendTextReply(wa.client, m, "Um, do I know you?")
	}
	if err != nil {
		log.Printf("Failed to send reply: %v", err)
		return
	}
}

func main() {
	clientID := os.Getenv("STRIDE_API_CLIENT_ID")
	clientSecret := os.Getenv("STRIDE_API_SECRET")
	webapp := &WebApp{
		client: stride.New(clientID, clientSecret),
	}
	mux := buildHandler(webapp)
	http.ListenAndServe("0.0.0.0:8080", mux)
}
