package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os"

	"bitbucket.org/atlassian/go-stride/pkg/stride"
)

type WebApp struct {
	client stride.Client
	store  recordStore
	// A per-user config item we store once globally for simplicity.
	configContent string
}

// recordStore is an interface for managing conversation records.
type recordStore interface {
	Get(conversationID string) (*conversationRecord, error)
	Put(*conversationRecord) error
	Delete(*conversationRecord) error
}

type conversationRecord struct {
	CloudID        string
	ConversationID string
	InstalledBy    string
}

// mapStore implements recordStore.
type mapStore struct {
	data map[string]*conversationRecord
}

func (s *mapStore) Get(conversationID string) (*conversationRecord, error) {
	if r, ok := s.data[conversationID]; ok {
		return r, nil
	}
	return nil, nil
}

func (s *mapStore) Put(record *conversationRecord) error {
	if s.data == nil {
		return fmt.Errorf("data map is nil")
	}
	s.data[record.ConversationID] = record
	return nil
}

func (s *mapStore) Delete(record *conversationRecord) error {
	delete(s.data, record.ConversationID)
	return nil
}

func buildHandler(wa *WebApp) http.Handler {
	r := http.NewServeMux()
	r.HandleFunc("/healthcheck", wa.healthcheck)
	r.HandleFunc("/descriptor", wa.descriptor)
	// Authenticated endpoints require a Bearer auth token which we validate.
	r.Handle("/installed", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.installed)))
	r.Handle("/uninstalled", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.uninstalled)))
	r.Handle("/bot-mention", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.botMention)))
	r.Handle("/conversation-updated", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.conversationUpdated)))
	r.Handle("/roster-updated", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.rosterUpdated)))
	// Redirects to static assets.
	r.Handle("/module/config", http.RedirectHandler("/static/app-module-config.html", http.StatusTemporaryRedirect))
	r.Handle("/module/dialog", http.RedirectHandler("/static/app-module-dialog.html", http.StatusTemporaryRedirect))
	r.Handle("/module/sidebar", http.RedirectHandler("/static/app-module-sidebar.html", http.StatusTemporaryRedirect))
	r.Handle("/img/icon.png", http.RedirectHandler("/static/icon.png", http.StatusTemporaryRedirect))
	// Serves state to frontend.
	r.Handle("/module/config/state", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.moduleConfigState)))
	r.Handle("/module/config/content", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.moduleConfigContent)))
	r.Handle("/module/glance/state", stride.RequireTokenMiddleware(wa.client, http.HandlerFunc(wa.moduleGlanceState)))
	r.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	// Uncomment this to see inbound requests.
	// return DumpRequest(r, true)
	return r
}

func (wa *WebApp) healthcheck(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	w.Write([]byte("OK"))
}

func (wa *WebApp) descriptor(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	descriptor := &stride.Descriptor{
		BaseURL: "https://" + req.Host,
		Key:     "refapp",
		LifeCycle: &stride.LifeCycle{
			Installed:   "/installed",
			Uninstalled: "/uninstalled",
		},
		Modules: map[stride.ModuleType][]stride.Module{
			stride.ModuleBot: {
				&stride.Bot{
					Key: "refapp-bot",
					Mention: &stride.URL{
						URL: "/bot-mention",
					},
					DirectMessage: &stride.URL{
						URL: "/bot-mention",
					},
				},
			},
			stride.ModuleInputAction: {
				&stride.InputAction{
					Key: "refapp-input-action",
					Name: stride.Name{
						Value: "Open App Dialog",
					},
					Target: "refapp-action-openDialog",
				},
			},
			stride.ModuleDialog: {
				&stride.Dialog{
					Key: "refapp-dialog",
					Title: stride.Name{
						Value: "App Dialog",
					},
					URL:            "/module/dialog",
					Authentication: "jwt",
					Options: stride.DialogOptions{
						Size: stride.DialogSize{
							Width:  "500px",
							Height: "300px",
						},
						PrimaryAction: stride.DialogAction{
							Key: "action-appendMessage",
							Name: stride.Name{
								Value: "Append Message",
							},
						},
						SecondaryActions: []stride.DialogAction{
							stride.DialogAction{
								Key: "action-close",
								Name: stride.Name{
									Value: "Close",
								},
							},
							stride.DialogAction{
								Key: "action-openSidebar",
								Name: stride.Name{
									Value: "Open Sidebar",
								},
							},
							stride.DialogAction{
								Key: "action-disableButton",
								Name: stride.Name{
									Value: "Disable Button",
								},
							},
						},
					},
				},
				&stride.Dialog{
					Key: "refapp-dialog-config",
					Title: stride.Name{
						Value: "App Configuration",
					},
					URL:            "/module/config",
					Authentication: "jwt",
					Options: stride.DialogOptions{
						Size: stride.DialogSize{
							Width:  "500px",
							Height: "300px",
						},
						PrimaryAction: stride.DialogAction{
							Key: "action-save",
							Name: stride.Name{
								Value: "Save",
							},
						},
						SecondaryActions: []stride.DialogAction{
							stride.DialogAction{
								Key: "action-close",
								Name: stride.Name{
									Value: "Close",
								},
							},
						},
					},
				},
			},
			stride.ModuleSidebar: {
				&stride.Sidebar{
					Key: "refapp-sidebar",
					Name: stride.Name{
						Value: "App Sidebar",
					},
					URL:            "/module/sidebar",
					Authentication: "jwt",
				},
			},
			stride.ModuleGlance: {
				&stride.Glance{
					Key: "refapp-glance",
					Name: stride.Name{
						Value: "App Glance",
					},
					Icon: stride.Icon{
						URL:   "/img/icon.png",
						URL2x: "/img/icon.png",
					},
					Target:         "refapp-action-openSidebar",
					QueryURL:       "/module/glance/state",
					Authentication: "jwt",
				},
			},
			stride.ModuleMessageAction: {
				&stride.MessageAction{
					Key: "refapp-message-action",
					Name: stride.Name{
						Value: "Send to Dialog",
					},
					Target: "refapp-action-openDialog",
				},
			},
			stride.ModuleConfiguration: {
				&stride.Configuration{
					Key: "refapp-config",
					Page: &stride.ConfigurationPage{
						Target: "refapp-dialog-config",
					},
					State: stride.URL{
						URL: "/module/config/state",
					},
					Authentication: "jwt",
				},
			},
			stride.ModuleWebhook: {
				&stride.Webhook{
					Key:   "refapp-webhook-conversation",
					Event: "conversation:updates",
					URL:   "/conversation-updated",
				},
				&stride.Webhook{
					Key:   "refapp-webhook-roster",
					Event: "roster:updates",
					URL:   "/roster-updated",
				},
			},
			stride.ModuleActionTarget: {
				&stride.ActionTarget{
					Key: "refapp-action-openDialog",
					OpenDialog: &stride.Key{
						Key: "refapp-dialog",
					},
				},
				&stride.ActionTarget{
					Key: "refapp-action-callService",
					CallService: &stride.URL{
						URL: "/module/action/refapp-service",
					},
				},
				&stride.ActionTarget{
					Key: "refapp-action-openSidebar",
					OpenSidebar: &stride.Key{
						Key: "refapp-sidebar",
					},
				},
			},
		},
	}
	b, err := json.Marshal(descriptor)
	if err != nil {
		// Failed to render descriptor. Should not happen.
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write(b)
}

func (wa *WebApp) installed(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Could not read request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// log.Println(string(b))
	var d stride.LifeCyclePayload
	err = json.Unmarshal(b, &d)
	if err != nil {
		log.Printf("Failed to decode json: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	record, _ := wa.store.Get(d.ResourceID)
	if record == nil {
		wa.store.Put(&conversationRecord{
			CloudID:        d.CloudID,
			ConversationID: d.ResourceID,
			InstalledBy:    d.UserID,
		})
		log.Printf("App installed in this conversation: %#v", d)
	} else {
		log.Printf("App already installed in conversation: %#v", d)
	}

	err = stride.SendText(wa.client, d.CloudID, d.ResourceID, "Hi there! Thanks for adding me to this conversation. To see me in action, just mention me in a message")
	if err != nil {
		log.Printf("Failed to send message: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println("App installed in a conversation")
	w.Write([]byte("Installed"))
}

func (wa *WebApp) uninstalled(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Could not read request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var d stride.LifeCyclePayload
	err = json.Unmarshal(b, &d)
	if err != nil {
		log.Printf("Failed to decode json: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	wa.store.Delete(&conversationRecord{ConversationID: d.ResourceID})
	log.Println("App uninstalled from a conversation")
	w.WriteHeader(http.StatusNoContent)
}

func (wa *WebApp) botMention(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	log.Println("Bot mention")

	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Could not read request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	m := &stride.Message{}
	err = json.Unmarshal(b, m)
	if err != nil {
		log.Printf("Failed to decode json: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = stride.SendTextReply(wa.client, m, "OK, I'm on it!")
	if err != nil {
		log.Printf("Failed to send reply: %v", err)
		return
	}
	// TODO: implement demos from nodejs refapp
	// 	.then(() => showCaseHighLevelFeatures({reqBody}))
	// .then(() => demoLowLevelFunctions({reqBody}))
	err = stride.SendTextReply(wa.client, m, "OK, I'm done. Thanks for watching!")
	if err != nil {
		log.Printf("Failed to send reply: %v", err)
		return
	}
}

// Handle the conversation:updates webhook.
func (wa *WebApp) conversationUpdated(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Could not read request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var d stride.ConversationUpdate
	err = json.Unmarshal(b, &d)
	if err != nil {
		log.Printf("Failed to decode json: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Printf("A conversation was changed: %s, change: %s", d.Conversation.ID, d.Action)

}

// Handle the roster:updates webhook.
func (wa *WebApp) rosterUpdated(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Could not read request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var d stride.RosterUpdate
	err = json.Unmarshal(b, &d)
	if err != nil {
		log.Printf("Failed to decode json: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Printf("A user joined or left a conversation: %s, change: %s", d.Conversation.ID, d.Action)
}

func (wa *WebApp) moduleConfigContent(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:
		wa.moduleConfigContentPost(w, req)
	case http.MethodGet:
		wa.moduleConfigContentGet(w, req)
	default:
		w.WriteHeader(http.StatusNotImplemented)
	}
}

func (wa *WebApp) moduleConfigContentPost(w http.ResponseWriter, req *http.Request) {
	// b, err := ioutil.ReadAll(req.Body)
	// if err != nil {
	// 	log.Printf("Could not read request body: %v", err)
	// 	w.WriteHeader(http.StatusInternalServerError)
	// 	return
	// }
	// // Custom data for frontend, not an inbuilt Stride type.
	// var d struct {
	// 	NotificationLevel string `json:"notificationLevel"`
	// }
	// err = json.Unmarshal(b, &d)
	// if err != nil {
	// 	log.Printf("Failed to decode json: %v", err)
	// 	w.WriteHeader(http.StatusInternalServerError)
	// 	return
	// }
	// wa.configContent = d.NotificationLevel
	req.ParseForm()
	wa.configContent = req.Form.Get("notificationLevel")
	w.WriteHeader(http.StatusNoContent)
	log.Printf("Updated config state: %s", wa.configContent)
}

func (wa *WebApp) moduleConfigContentGet(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Content-Type", "application/json")
	// TODO: retrieve state from store
	d := struct {
		NotificationLevel string `json:"notificationLevel"`
	}{
		NotificationLevel: wa.configContent,
	}
	b, err := json.Marshal(d)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write([]byte(b))
}

func (wa *WebApp) moduleConfigState(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:
		wa.moduleConfigStatePost(w, req)
	case http.MethodGet:
		wa.moduleConfigStateGet(w, req)
	default:
		w.WriteHeader(http.StatusNotImplemented)
	}
}

func (wa *WebApp) moduleConfigStatePost(w http.ResponseWriter, req *http.Request) {
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Could not read request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var d stride.RosterUpdate
	err = json.Unmarshal(b, &d)
	if err != nil {
		log.Printf("Failed to decode json: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Printf("A user joined or left a conversation: %s, change: %s", d.Conversation.ID, d.Action)
}

func (wa *WebApp) moduleConfigStateGet(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Content-Type", "application/json")
	// TODO: retrieve state from store
	w.Write([]byte("{}"))
}

func (wa *WebApp) moduleGlanceState(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	state := &stride.GlanceBody{
		Label: stride.GlanceLabel{
			Value: "Click me!",
		},
	}
	b, err := json.Marshal(state)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func main() {
	clientID := os.Getenv("STRIDE_API_CLIENT_ID")
	clientSecret := os.Getenv("STRIDE_API_SECRET")
	webapp := &WebApp{
		client: stride.New(clientID, clientSecret),
		store: &mapStore{
			data: make(map[string]*conversationRecord),
		},
	}
	mux := buildHandler(webapp)
	http.ListenAndServe("0.0.0.0:8080", mux)
}

// DumpRequest prints request metadata, headers and (optional) body to stdout.
func DumpRequest(next http.Handler, includeBody bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dumpRequest(r, includeBody)
		next.ServeHTTP(w, r)
	})
}

func dumpRequest(r *http.Request, includeBody bool) {
	requestDump, err := httputil.DumpRequest(r, true)
	if err != nil {
		fmt.Printf("Failed to dump request: %v\n", err)
		return
	}
	fmt.Printf("============================================================\n")
	if includeBody {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Printf("Failed to read request body: %v\n", err)
			return
		}
		r.Body.Close()
		fmt.Println(string(requestDump))
		r.Body = ioutil.NopCloser(bytes.NewReader(body))
	}
	fmt.Printf("============================================================\n\n\n")
}
